//
//  Entities.swift
//  App
//
//  Created by Wayne Eldridge on 2019/08/04.
//

import Vapor

struct BitcoinSummary: Content, Codable {
    var headlines: [String]
    var score: String
    var sources: [String]
}

struct Sentiment: Content, Codable {
    let text: String
}

struct NewsResponse: Codable, Content {
    var status: String?
    var articles: [NewArticle]?
    
    init(fromData: Data) {
        let decoder = JSONDecoder()
        do {
            self = try decoder.decode(NewsResponse.self, from: fromData)
        } catch let error {
            print("Issue with mapping cell data: \(error)")
        }
    }
}

struct NewArticle: Codable, Content {
    var title: String
    var url: String
    var source: Source?
}

struct Source: Codable {
    var name: String?
}

struct NaturalLanguageBody: Codable, Content, ResponseEncodable {
    var encodingType: String
    var document: Document
}

struct Document: Content, Codable {
    var type: String
    var content: String
}

struct NaturalResponse: Codable {
    var language: String
    var documentSentiment: DocumentSentiment
}

struct DocumentSentiment: Codable {
    var score: Double
}


struct NaturalLanguage: Codable {
    var language: String?
    var documentSentiment: Score?
    
    init(fromData: Data) {
        let decoder = JSONDecoder()
        do {
            self = try decoder.decode(NaturalLanguage.self, from: fromData)
        } catch let error {
            print("Issue with mapping cell data: \(error)")
        }
    }
    
}

struct Score: Codable {
    var score: Double
}

