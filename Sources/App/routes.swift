import Vapor
import SwiftyRequest

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // MARK: - Home
    router.get {req -> Future<View> in
        getAllNews()
        return try req.view().render("home")
    }
    
    // MARK: - Individual Text
    router.post(Sentiment.self, at: "text") { req, article  -> Future<View> in
        let naturalLanguagAPIResponse =  try req.client().post(naturalLanguageEndpoint) { request in
            let document = Document(type: "PLAIN_TEXT", content: article.text)
            let naturalLanguageBody = NaturalLanguageBody(encodingType: "UTF8", document: document)
            return try request.content.encode(naturalLanguageBody)
        }
        
        let narutalResponse = naturalLanguagAPIResponse.flatMap {response in
            return try response.content.decode(NaturalResponse.self)
        }
        
        let textSentimentScore = narutalResponse.map(to: String.self) { content in
            let sentimentScore = content.documentSentiment.score.description
            return sentimentScore
        }
        
        return try req.view().render("text",  ["score": textSentimentScore])
    }
    
    // MARK: - Bitcoin News
    router.post("bitcoin") { req  -> Future<View> in
        let bitcoinPage = BitcoinSummary(headlines: newsTitles, score: score, sources: newsURLS)
        return try req.view().render("bitcoin",  bitcoinPage)
    }
}
