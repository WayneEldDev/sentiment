//
//  NewsControl.swift
//  App
//
//  Created by Wayne Eldridge on 2019/08/04.
//

import Vapor
import SwiftyRequest

// News Summary
var newsTitlesConcatenated = String()
var newsTitles = [String]()
var newsURLS = [String]()
var score = String()
let searchQuery = "Bitcoin"
let naturalLanguageEndpoint = "https://language.googleapis.com/v1/documents:analyzeSentiment?key=AIzaSyDBNoyvA2WtMdb3QrlCWfPDi4SGEViKlW0"


// MARK: - Get News
func getAllNews() {
    let request = RestRequest(url: "https://newsapi.org/v2/everything?q=\(searchQuery)&from=2019-08-02&sortBy=popularity&apiKey=b9aa45ad3f97489b80e3d86f653469e0")
    request.responseData(completionHandler: { (response) in
        
        switch response.result {
        case .success(let result):
            let newsResponse = NewsResponse(fromData: result)
            if let articles = newsResponse.articles {
                var titles = [String]()
                articles.forEach({ (article) in
                    titles.append(article.title)
                    newsTitles.append(article.title)
                    newsURLS.append("Source: \(article.source?.name ?? "") URL: \(article.url)")
                })
                newsTitlesConcatenated = titles.joined(separator: " ")
                getScoreOfNews()
                print("News completed")
            }
        case .failure(let error):
            print(error.localizedDescription)
        }
    })
}

// MARK: - Get News Score
func getScoreOfNews() {
    let document = Document(type: "PLAIN_TEXT", content: newsTitlesConcatenated)
    let naturalLanguageBody = NaturalLanguageBody(encodingType: "UTF8", document: document)
    let encoder = JSONEncoder()
    
    do {
        let data = try encoder.encode(naturalLanguageBody)
        let request = RestRequest(method: .post ,url: naturalLanguageEndpoint)
        request.messageBody = data
        request.responseData(completionHandler: { (response) in
            switch response.result {
            case .success(let result):
                let response = NaturalLanguage(fromData: result)
                score = response.documentSentiment?.score.description ?? "No Score"
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    } catch {
        print(error.localizedDescription)
    }
}

