// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "Sentiment",
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),

        // 🍃 An expressive, performant, and extensible templating language built for Swift.
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.0"),
        .package(url: "https://github.com/IBM-Swift/SwiftyRequest.git", from: "2.1.1"),
        .package(url: "https://github.com/IBM-Swift/LoggerAPI.git", .upToNextMinor(from: "1.8.0"))
    ],
    targets: [
        .target(name: "App", dependencies: ["Leaf", "Vapor"]),
        .target(name: "Run", dependencies: ["App", "SwiftyRequest"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

