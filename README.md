#Sentiment Analysis Application 
The purpose fo this exercise was to deploy an application to Google Cloud Platform to showcase a more realistic version of my skill set.

The main purpose of the application is to discern a news topic’s current sentiment from the latest news headlines. One could argue that cryptocurrencies are traded with regards to the news surrounding them.  In the case of this application the main purpose was to discern the sentiment for the latest articles for Bitcoin. I.e take the latest Bitcoin headlines, pass them through Googles Natural Language API and receive an average sentiment score regarding that news, to possibly trade on.

Please see the following link for the Sentiment Analysis Application:  https://sentiment-hkl3qxp65q-uc.a.run.app.

In the application you will see a text area where you can test individual text sentiment. There is also a button at the bottom that takes you to the analysis of several Bitcoin news articles, their content, sources and average sentiment analysis score.

In short the following application used:
- Google Cloud Run (beta)
- Google Kubernetes Engine
- Google Container Registry
- Google Cloud Natural Language API
- Docker
- Vapor (Server Side Swift)
- Gnews API

The application retrieved headlines of Bitcoin news articles from a Gnews API and passed it into Google’s Natural Language API for a sentiment analysis and further overall score on the headlines. (Providing a positive or negative sentiment for Bitcoin news headlines).

Google Cloud Run was used as a stateless container to serve the application using Kubernetes Engine clusters for computing power, application management, stability and load balancing.

A docker container image was built off a Vapor (Server side Swift backend) application, to construct the application logic and provide a Vapor Leaf templating frontend for visibility of results. 

The docker container images was then registered with the Google Cloud Registry and later deployed to Google Cloud Run.
